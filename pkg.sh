#!/bin/sh
set -eu

file_name=ncgdmw-lua.zip

cleanup() {
    for f in CHANGELOG.md EVENTS_AND_INTERFACE.md FAQ.md LICENSE README.md; do
        mv docs/$f .
    done
}

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

for f in CHANGELOG.md EVENTS_AND_INTERFACE.md FAQ.md LICENSE README.md; do
    mv $f docs/
done

zip --must-match --recurse-paths $file_name \
    docs l10n scripts ncgdmw_alt_start.omwaddon ncgdmw_starwind.omwaddon ncgdmw.omwaddon ncgdmw.omwscripts version.txt \
    || cleanup
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt

cleanup
