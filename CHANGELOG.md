## NCGDMW Lua Edition Changelog

#### 1.3

* Added the `debugMode` option which enables extra debugging info in the console.
* Fixed a bug that broke the decay stats menu. ([#20](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/20))
* Fixed a bug where decay progress wouldn't halve when you raised a skill. ([#19](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/19))
* Fixed typo that broke getting the decay rate. ([#20](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/20))

<!-- [Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15913322) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136) -->

#### 1.2

* Fixed typo that could break the stats menu for Starwind players

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15913322) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.1

* The mod now updates when the UI is opened and can immediately see when barter and/or speechcraft are raised (`onUpdate` -> `onFrame` under the hood; latency is critical for this mod so it is OK to use this engine handler).
* When used with OpenMW 0.49 or newer this mod can now directly examine loaded plugins to ensure that the required `.omwaddon` is loaded (versus the prior method of looking at GMST values, which will still be used for OpenMW 0.48).
* Fixed a problem where external changes to Luck were not preserved ([#14](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/14)).

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15883628) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.0

* The mod is now considered out of beta! This is simply a re-release of beta12 with no changes.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15708304) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### beta12

* Fixed incorrect handling of leveling down to 1

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15383561)

#### beta11

* Fixed more incorrect Starwind skill names

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11285260)

#### beta10

* Fixed an incorrect Starwind skill name that I missed

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11255731)

#### beta9

* Fixed a bug that caused the alt start/Starwind plugins to infinitely spawn potions

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11083945)

#### beta8

* Fixed some incorrect Starwind skill names

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11037746)

#### beta7

* The stats menus now update while you have them open.
* An error is now shown in the in-game UI when a required `.omwaddon` file isn't detected.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/10564152)

#### beta6

* The alt start plugin now doesn't depend on Morrowind master files, in order to support games that don't depend on `Morrowind.esm` and friends.
* Added a specific plugin for [**Starwind**](https://www.nexusmods.com/morrowind/mods/48909) that doesn't provide descriptions for skills; the existing alt-start plugin can be used for other non-Morrowind games or alternative chargen mods.
* Added support for Starwind skill names in the NCGD skill decay menu

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/9792120)

#### beta5

* Added support for [**Starwind**](https://www.nexusmods.com/morrowind/mods/48909) (as well as any other non-Morrowind game or non-vanilla chargen mod) via an "alt start" plugin (credit goes to Greywander for the original alt start implementation)
  * This will place a potion in your inventory that you can drink to activate NCGDMW
  * Drink this potion **after** you've selected your race, class, and sign
* `build.sh` will now fail if we try to zip something that isn't there (`zip --must-match ...`)
* Correctly implemented the PL and PT_BR localizations
* Removed outdated interface documentation
* Added contact information to the README

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/8354345)

#### beta4

* Support external changes to attributes (for instance if another mod raises them, or if the player becomes a vampire) on top of NCGDMW's own calculations
* Fixed a bug that caused extremely rapid decay rates
* Added a note about mod compatibility with NCGDMW Lua Edition and Ability vs Curse spell types
* Updated the FAQ entry about going past 100 to clarify the current state of that
* Removed `Skill()` from the interface
* Added a note about this history of NCGDMW to the FAQ

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/7514203)

#### beta3

* Support new YAML-formatted localizations
* Added localizations for BR
* Added an interface for other mods to interact with NCGDMW
* Added documentation for events and the interface to the website
* Updated the mod settings menu to use the new builtin renderers provided by OpenMW
* The decay stats menu has a new layout and displays additional information
* The stats menu (with or without decay) will now display your [Marksman's Eye](https://modding-openmw.gitlab.io/marksmans-eye/) level, if you're also using that mod

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/7081151)

#### beta2

* Fixed a crash on skill up ([#1](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/1))
* Fixed health adjustments not applying after saving and reloading ([#2](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/2))
* Changed the layout of the zip file a bit
* Added changelog and faq pages to the website

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/6074455)

#### beta1

Initial public release, now compatible with the latest code in OpenMW 0.48 dev builds.

No new features were added for this release; the only changes involved porting code for updated APIs.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5829792)

#### alpha1

Initial release of the mod, written for WIP code that had not yet been merged into OpenMW.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5356693)
