## Events & Interface

This page describes how to hook into NCGDMW Lua Edition from other mods.

#### Events

There are no events at this time.

#### Interface

##### Examples

Some simple usage examples are featured below.

```lua
local I = require("openmw.interfaces")
local ui = require("openmw.ui")

local function maybeRaiseSpeed()
  if I.NCGDMW then
    local currrent = I.NCGDMW.Attribute("speed")
    -- This will trigger an on-screen message
    I.NCGDMW.Attribute("speed", current + 1)
  end
end

local function ncgdCurrentDecayRate()
  if I.NCGDMW then
    local msg = "NCGDMW Decay Rate: %s"
    local rate = I.NCGDMW.DecayRate(nil, true)
    ui.showMessage(string.format(msg, rate))
  end
end

local function ncgdLevelProgress()
  if I.NCGDMW then
    local msg = "NCGDMW Level Progress: %s"
    local prog = I.NCGDMW.LevelProgress()
    ui.showMessage(string.format(msg, prog))
  end
end
```

##### `Attribute()`

Args: `name, val`

Give it just the `name` arg to get the value of that attribute.

Give it `name` as well as `val` to set the given attribute to the given value.

##### `LevelProgress()`

Returns the current level progress as a string representation of a number with a percent sign.
