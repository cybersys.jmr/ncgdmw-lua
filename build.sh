#!/bin/sh
set -e

if [ -n "$CI" ]; then
    mkdir -p "$HOME"/.config/openmw
    echo "data=\"/srv\"" > "$HOME"/.config/openmw/openmw.cfg
fi

target=${1-none}

if [ "$target" = none ]; then
    # Build 'em all
    delta_plugin convert ncgdmw.yaml
    cd ncgdmw_alt_start.d
    delta_plugin convert ncgdmw_alt_start.yaml
    mv ncgdmw_alt_start.omwaddon ..
    cd ../ncgdmw_starwind.d
    delta_plugin convert ncgdmw_starwind.yaml
    mv ncgdmw_starwind.omwaddon ..
else
    if [ "$target" = "ncgdmw" ]; then
        delta_plugin convert "$target".yaml
    else
        cd "$target".d
        delta_plugin convert "$target".yaml
        mv "$target".omwaddon ..
    fi
fi
